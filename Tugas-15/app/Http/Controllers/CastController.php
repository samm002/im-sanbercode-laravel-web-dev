<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index()
    {
        $cast = DB::table('cast')->get();

        return view('websiteTugas15.cast.showAllCast', ['cast' => $cast]);
    }

    public function create()
    {
        return view('websiteTugas15.cast.createCast');
    }

    public function store(Request $request)
    {
        // validasi data input
        $request->validate([
            'nama' => 'required|min:3|max:255',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        
        // input data ke database (tabel cast)
        DB::table('cast')->insert([
            'nama' => $request->input('nama'),
            'umur' => $request->input('umur'),
            'bio' => $request->input('bio'),
        ]);
        return redirect('/cast');
    }
    
    public function show($cast_id)
    {
        $castById = DB::table('cast')->find($cast_id);
        
        return view('websiteTugas15.cast.showCastById', ['castById' => $castById]);
    }
    
    public function edit($cast_id)
    {
        $castById = DB::table('cast')->find($cast_id);
        
        return view('websiteTugas15.cast.editCastById', ['castById' => $castById]);
    }
    
    public function update(Request $request, $cast_id)
    {
        $request->validate([
            'nama' => 'required|min:3|max:255',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        
        DB::table('cast')
        ->where('id', $cast_id)
        ->update(
            [
                'nama' => $request->input('nama'),
                'umur' => $request->input('umur'),
                'bio' => $request->input('bio'),
                ]
            );

            return redirect('/cast');    
    }
            
    public function destroy($cast_id)
    {
        DB::table('cast')->where('id', $cast_id)->delete();

        return redirect('/cast');
    }
}
            