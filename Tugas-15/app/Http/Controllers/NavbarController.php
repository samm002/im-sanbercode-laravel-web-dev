<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NavbarController extends Controller
{
    public function table()
    {
        return view('websiteTugas13.table');
    }

    public function dataTables()
    {
        return view('websiteTugas13.data-tables');
    }

    
}
