@extends('websiteTugas13.layouts.master')

@section('title')
Show Cast by Id = {{$castById->id}}
@endsection

@section('content')

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{$castById->nama}}</td>
            <td>{{$castById->umur}}</td>
            <td>{{$castById->bio}}</td>
            <td>
                <a href="/cast/{{$castById->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
            </td>
          </tr>
</table>
<a href="/cast" class="btn btn-sm btn-info my-3">Kembali</a>

@endsection