@extends('websiteTugas13.layouts.master')

@section('title')
Create Cast Form
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" class="@error('nama') is-invalid @enderror form-control" placeholder="Masukan Nama Pemeran (Cast)">
    </div>

    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Umur</label>
        <input type="text" name="umur" class="@error('umur') is-invalid @enderror form-control" placeholder="Masukan Umur Pemeran Sekarang (Cast)">
    </div>
    
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
        <label>Bio</label>
        <textarea type="text" name="bio" class="@error('bio') is-invalid @enderror form-control" cols="30" rows="10" placeholder="Masukan Bio Pemeran (Cast)"></textarea>
    </div>

    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection