<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register () 
    {
        return view('websiteTugas12.register');
    }
    public function welcomes (Request $request) 
    {
        $firstName = $request->input("fname");
        $lastName = $request->input("lname");
        return view('websiteTugas12.welcomes', ['firstName' => $firstName, 'lastName' => $lastName]);
    }
}
