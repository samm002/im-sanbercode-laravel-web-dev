<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NavbarController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Routing Tugas 12
Route::get('/', [HomeController::class, 'home']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/welcomes', [AuthController::class, 'welcomes']);

// Routing Tugas 13
Route::get('/table', [NavbarController::class, 'table']);
Route::get('/data-tables', [NavbarController::class, 'dataTables']);

// Routing cara lain
// Route::get('/table', function(){
//     return view('websiteTugas13.table');
// });
// Route::get('/data-tables', function(){
//     return view('websiteTugas13.data-tables');
// });
