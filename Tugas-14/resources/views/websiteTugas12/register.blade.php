@extends('websiteTugas13.layouts.master')

@section('title')
Register
@endsection

@section('content')
<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>
<form action="/welcomes" method="post">
    @csrf
    <label>First name:</label><br><br>
    <input type="text" name="fname"><br><br>
    <label>Last name:</label><br><br>
    <input type="text" name="lname"><br><br>
    <label>Gender:</label><br><br>
    <input type="radio" name="gender" value="Male">Male<br>
    <input type="radio" name="gender" value="Female">Female<br>
    <input type="radio" name="gender" value="Other">Other<br><br>
    <label>Nationality:</label><br><br>
    <select name="nation">
        <option value="Indonesia">Indonesian</option>
        <option value="Singapore">Singaporean</option>
        <option value="Malaysia">Malaysian</option>
        <option value="Australia">Australian</option>
    </select><br><br>
    <label>Language Spoken:</label><br><br>
    <input type="checkbox" name="languages[]" value="Bahasa Indonesia">Bahasa Indonesia<br>
    <input type="checkbox" name="languages[]" value="English">English<br>
    <input type="checkbox" name="languages[]" value="Other">Other<br><br>
    <label>Bio:</label><br><br>
    <textarea name="teks bio" cols="30" rows="10"></textarea><br>
    <input type="submit" value="Sign Up">
</form>
@endsection