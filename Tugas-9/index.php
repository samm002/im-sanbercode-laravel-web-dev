<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OOP</title>
</head>

<body>
    <?php
    require_once "animal.php";
    require_once "Ape.php";
    require_once "Frog.php";

    // Release 0
    $sheep = new Animal("shaun");
    
    // echo $sheep->name; // "shaun"
    // echo $sheep->legs; // 4
    // echo $sheep->cold_blooded; // "no"
    
    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
    $sheep->get_name();
    $sheep->get_legs();
    $sheep->get_cold_blooded();
    
    echo "<br>";
    
    // Release 1
    $kodok = new Frog("buduk");
    $kodok->get_name();
    $kodok->get_legs();
    $kodok->get_cold_blooded();
    $kodok->jump();

    echo "<br>";

    $sungokong = new Ape("kera sakti");
    $sungokong->get_name();
    $sungokong->get_legs();
    $sungokong->get_cold_blooded();
    $sungokong->yell();

    ?>

</body>

</html>