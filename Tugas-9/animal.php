<?php
    class Animal
    {
        protected $name;
        protected $legs = 4;
        protected $cold_blooded = "no";
        
        public function __construct($name) 
        {
            $this->name= $name;
        }

        public function get_name() {
            echo "Name : " . $this->name . "<br>";
        }

        public function get_legs() {
            echo "legs : " . $this->legs . "<br>";
        }

        public function get_cold_blooded() {
            echo "cold blooded : " . $this->cold_blooded . "<br>";
        }
    }
?>