<?php

require_once "animal.php";

class Frog extends Animal
{
    protected $jump_sound = "Hop Hop";
    public function jump() {
        echo "Jump : " . $this->jump_sound . "<br>";
    }
}

?>
