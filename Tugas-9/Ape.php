<?php

require_once "animal.php";

class Ape extends Animal
{
    protected $yell_sound = "Auooo";
    public function __construct($name)
    {
        parent::__construct($name);
        $this->legs = 2;
    }
    public function yell() {
        echo "Yell : " . $this->yell_sound . "<br>";
    }
}

?>
