<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register () 
    {
        return view('websiteTugas12.register');
    }
    public function welcome (Request $request) 
    {
        $firstName = $request->input("fname");
        $lastName = $request->input("lname");
        return view('websiteTugas12.welcome', compact('firstName', 'lastName'));
    }
}
