<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register () 
    {
        return view('websiteTugas1.register');
    }
    public function welcome (Request $request) 
    {
        $firstName = $request->input("fname");
        $lastName = $request->input("lname");
        return view('websiteTugas1.welcome', ['firstName' => $firstName, 'lastName' => $lastName]);
    }
}
